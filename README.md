# SYNOPSIS 
[![NPM Package](https://img.shields.io/npm/v/pull-leb128.svg?style=flat-square)](https://www.npmjs.org/package/pull-leb128)
[![Build Status](https://img.shields.io/travis/wanderer/pull-leb128.svg?branch=master&style=flat-square)](https://travis-ci.org/wanderer/pull-leb128)
[![Coverage Status](https://img.shields.io/coveralls/wanderer/pull-leb128.svg?style=flat-square)](https://coveralls.io/r/wanderer/pull-leb128)

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)  

[LEB128](https://en.wikipedia.org/wiki/LEB128) encoding and decoding for signed and unsinged intergers. Supports arbitary length intergers larger then `Number.MAX_SAFE_INTEGER`

# INSTALL
`npm install pull-leb128`

# USAGE
```javascript
const leb = require('leb128')
const pull = require('pull-stream')
const Pushable = require('pull-pushable')
const Reader = require('pull-reader')

const writer = Pushable()
const reader = Reader()
pull(writer, reader)

leb.unsigned.encode('9019283812387', writer)
leb.unsigned.read(reader).then(decoded => {
   console.log(decoded)
  // 9019283812387
})
```

# API
Use `require('leb128/signed')` for signed encoding and decoding and 
`require('leb128/unsigned')` for unsigned methods

## unsigned.write

[unsigned.js:53-65](https://github.com/wanderer/pull-leb128/blob/52df553636e097e85c310db130ca313fbc5959d2/unsigned.js#L53-L65 "Source code on GitHub")

Writes a number as an unsigned leb128 to an instance of [pull-pushable](https://github.com/pull-stream/pull-pushable)

**Parameters**

-   `number` **([String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) \| [Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number))** 
-   `writer` **pull-pushable** 

## unsigned.read 

[unsigned.js:4-8](https://github.com/wanderer/pull-leb128/blob/52df553636e097e85c310db130ca313fbc5959d2/unsigned.js#L4-L8 "Source code on GitHub")

Reads an unsigned leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)

**Parameters**

-   `reader` **pull-read** 

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** the promise resolves with a string containing the decoded integer

## unsigned.readBn

[unsigned.js:24-46](https://github.com/wanderer/pull-leb128/blob/52df553636e097e85c310db130ca313fbc5959d2/unsigned.js#L24-L46 "Source code on GitHub")

Reads an unsigned leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)

**Parameters**

-   `reader` **pull-read** 

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** the promise resolves with a [bn.js](https://github.com/indutny/bn.js/) containing the decoded integer

## signed.write

[signed.js:56-78](https://github.com/wanderer/pull-leb128/blob/52df553636e097e85c310db130ca313fbc5959d2/signed.js#L56-L78 "Source code on GitHub")

Writes a number as a signed leb128 to an instance of [pull-pushable](https://github.com/pull-stream/pull-pushable)

**Parameters**

-   `num`  
-   `writer` **pull-pushable** 
-   `number` **([String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) \| [Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number))** 

## signed.read

[signed.js:4-8](https://github.com/wanderer/pull-leb128/blob/52df553636e097e85c310db130ca313fbc5959d2/signed.js#L4-L8 "Source code on GitHub")

Reads a signed leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)

**Parameters**

-   `reader` **pull-read** 

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** the promise resolves with a string containing the decoded integer

## signed.readBn

[signed.js:24-49](https://github.com/wanderer/pull-leb128/blob/52df553636e097e85c310db130ca313fbc5959d2/signed.js#L24-L49 "Source code on GitHub")

Reads a signed leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)

**Parameters**

-   `reader` **pull-read** 

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** the promise resolves with a [bn.js](https://github.com/indutny/bn.js/) containing the decoded integer


# LICENSE
[MPL-2.0](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2))

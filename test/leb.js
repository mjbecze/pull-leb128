const tape = require('tape')
const leb = require('../')
const pull = require('pull-stream')
const Pushable = require('pull-pushable')
const Reader = require('pull-reader')

tape('leb - round trips', async t => {
  const writer = Pushable()
  let reader = Reader()
  leb.unsigned.write(8, writer)
  pull(writer, reader)

  let buffer = await new Promise((resolve, reject) => {
    reader.read(1, (err, val) => resolve(val))
  })
  t.equals(buffer.toString('hex'), '08')

  leb.signed.write('-9223372036854775808', writer)

  buffer = await new Promise((resolve, reject) => {
    reader.read(10, (err, val) => resolve(val))
  })

  t.equals(buffer.toString('hex'), '8080808080808080807f')

  writer.push(buffer)
  let val = await leb.signed.read(reader)
  t.equals(val, '-9223372036854775808')

  leb.signed.write('-100', writer)
  buffer = await new Promise((resolve, reject) => {
    reader.read(2, (err, val) => resolve(val))
  })

  t.equals(buffer.toString('hex'), '9c7f')
  writer.push(buffer)

  val = await leb.signed.read(reader)
  t.equals(val, '-100')

  leb.signed.write('100', writer)
  buffer = await new Promise((resolve, reject) => {
    reader.read(2, (err, val) => resolve(val))
  })

  t.equals(buffer.toString('hex'), 'e400')
  writer.push(buffer)
  val = await leb.signed.read(reader)

  t.equals(val, '100')

  leb.signed.write('10', writer)

  buffer = await new Promise((resolve, reject) => {
    reader.read(1, (err, val) => resolve(val))
  })

  t.equals(buffer.toString('hex'), '0a')
  writer.push(buffer)
  val = await leb.signed.read(reader)

  t.equals(val, '10')

  leb.signed.write('2141192192', writer)
  buffer = await new Promise((resolve, reject) => {
    reader.read(5, (err, val) => resolve(val))
  })
  t.equals(buffer.toString('hex'), '808080fd07')

  writer.push(buffer)
  val = await leb.signed.read(reader)
  t.equals(val, '2141192192')

  leb.unsigned.write('2141192192', writer)
  buffer = await new Promise((resolve, reject) => {
    reader.read(5, (err, val) => resolve(val))
  })
  t.equals(buffer.toString('hex'), '808080fd07')

  writer.push(buffer)
  val = await leb.unsigned.read(reader)
  t.equals(val, '2141192192')

  let err
  try {
    writer.push(Buffer.from('808080fd', 'hex'))
    writer.end()
    val = await leb.unsigned.read(reader)
  } catch (e) {
    err = e
  } finally {
    t.equals(err.message, 'stream ended before int could decoded: true')
  }

  try {
    writer.push(Buffer.from('808080fd', 'hex'))
    writer.end()
    val = await leb.signed.read(reader)
  } catch (e) {
    err = e
  } finally {
    t.equals(err.message, 'stream ended before int could decoded: true')
  }

  t.end()
})

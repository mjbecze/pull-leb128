const Bn = require('bn.js')
const Buffer = require('safe-buffer').Buffer

module.exports = {
  write,
  read,
  readBn
}

/**
 * Reads a signed leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)
 * @param {pull-read} reader
 * @returns {Promise} the promise resolves with a string containing the decoded integer
 */
function read (reader) {
  return readBn(reader).then(bn => bn.toString())
}

/**
 * Reads a signed leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)
 * @param {pull-read} reader
 * @returns {Promise} the promise resolves with a [bn.js](https://github.com/indutny/bn.js/) containing the decoded integer
 */
async function readBn (reader) {
  const num = new Bn(0)
  let shift = 0
  let byt
  while (true) {
    byt = await new Promise((resolve, reject) => {
      reader.read(1, (err, byt) => {
        if (err) {
          reject(new Error(`stream ended before int could decoded: ${err}`))
        } else {
          resolve(byt[0])
        }
      })
    })
    num.ior(new Bn(byt & 0x7f).shln(shift))
    shift += 7
    if (byt >> 7 === 0) {
      break
    }
  }
  // sign extend if negitive
  if (byt & 0x40) {
    num.setn(shift)
  }
  return num.fromTwos(shift)
}

/**
 * Writes a number as a signed leb128 to an instance of [pull-pushable](https://github.com/pull-stream/pull-pushable)
 * @param {String|Number} number
 * @param {pull-pushable} writer
 */
function write (num, writer) {
  num = new Bn(num)
  const isNeg = num.isNeg()
  if (isNeg) {
    // add 8 bits for padding
    num = num.toTwos(num.bitLength() + 8)
  }
  while (true) {
    const i = num.maskn(7).toNumber()
    num.ishrn(7)
    if ((isNegOne(num) && (i & 0x40) !== 0) ||
      (num.isZero() && (i & 0x40) === 0)) {
      writer.push(Buffer.from([i]))
      break
    } else {
      writer.push(Buffer.from([i | 0x80]))
    }
  }

  function isNegOne (num) {
    return isNeg && num.toString(2).indexOf('0') < 0
  }
}

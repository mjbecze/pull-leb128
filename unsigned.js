const Bn = require('bn.js')
const Buffer = require('safe-buffer').Buffer

module.exports = {
  read,
  readBn,
  write
}

/**
 * Reads an unsigned leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)
 * @param {pull-read} reader
 * @returns {Promise} the promise resolves with a string containing the decoded integer
 */
function read (reader) {
  return readBn(reader).then(bn => bn.toString())
}

/**
 * Reads an unsigned leb128 from an instance of [pull-read](https://github.com/dominictarr/pull-reader)
 * @param {pull-read} reader
 * @returns {Promise} the promise resolves with a [bn.js](https://github.com/indutny/bn.js/) containing the decoded integer
 */
async function readBn (reader) {
  const num = new Bn(0)
  let shift = 0
  let byt
  while (true) {
    byt = await new Promise((resolve, reject) => {
      reader.read(1, (err, byt) => {
        if (err) {
          reject(new Error(`stream ended before int could decoded: ${err}`))
        } else {
          resolve(byt[0])
        }
      })
    })
    num.ior(new Bn(byt & 0x7f).shln(shift))
    if (byt >> 7 === 0) {
      break
    } else {
      shift += 7
    }
  }
  return num
}

/**
 * Writes a number as an unsigned leb128 to an instance of [pull-pushable](https://github.com/pull-stream/pull-pushable)
 * @param {String|Number} number
 * @param {pull-pushable} writer
 */
function write (number, writer) {
  const num = new Bn(number)
  while (true) {
    const i = num.maskn(7).toNumber()
    num.ishrn(7)
    if (num.isZero()) {
      writer.push(Buffer.from([i]))
      break
    } else {
      writer.push(Buffer.from([i | 0x80]))
    }
  }
}
